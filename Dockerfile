FROM centos
LABEL MAINTAINER gustavo herrera <gustavoh.2312@gmail.com>
RUN mkdir /opt/tomcat/
WORKDIR /opt/tomcat
RUN curl -O https://archive.apache.org/dist/tomcat/tomcat-8/v8.0.3/bin/apache-tomcat-8.0.3.tar.gz
RUN tar xvfz apache*.tar.gz
RUN mv apache-tomcat-8.0.3/* /opt/tomcat/.
RUN yum -y install java-1.8.0-openjdk
RUN java -version
WORKDIR /opt/tomcat/conf
COPY tomcat-users.xml .
WORKDIR /opt/tomcat/webapps
#RUN curl -O -L https://github.com/AKSarav/SampleWebApp/raw/master/dist/SampleWebApp.war
COPY PSE_TEST.war .
EXPOSE 8880 8100
WORKDIR /
RUN mkdir /cpe_test
WORKDIR /cpe_test
RUN mkdir cpe_transito_test

# instalar gcsfuse
RUN echo $'[gcsfuse] \n\
name=gcsfuse (packages.cloud.google.com) \n\
baseurl=https://packages.cloud.google.com/yum/repos/gcsfuse-el7-x86_64 \n\
enabled=1 \n\
gpgcheck=1 \n\
repo_gpgcheck=1 \n\
gpgkey=https://packages.cloud.google.com/yum/doc/yum-key.gpg \n\
       https://packages.cloud.google.com/yum/doc/rpm-package-key.gpg' > /etc/yum.repos.d/gcsfuse.repo

RUN \
    yum update -y && \
    yum install -y gcsfuse && \
    yum install -y wget tree bzip2
RUN mkdir -p /etc/letsencrypt

CMD ["/opt/tomcat/bin/catalina.sh", "run"]